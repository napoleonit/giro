// Gyroscope-controlled camera for iPhone & Android revised 2.26.12

// Perry Hoberman <hoberman@bway.net>

using UnityEngine;
using System;

public class GiroHandler : MonoBehaviour

{
    private bool gyroBool;
    private Gyroscope gyro;
    private Quaternion rotFix;
 	float S = 0.0f;
	float V = 0.0f;
	float V0 = 0.0f;
	double accel = 0.0f;
	float viscosity = 0.0f;
	float rotationModule = 0.0f;
	Vector3 prevRot = Vector3.zero;
	float lastTime;
	float timeInv;
	float dtOut = 0.0f;
	public float updateInterval = 0.5F;
    private double lastInterval;
	double maxAccel = 0.0f;
	double minAccel = 0.0f;
	double amount=0;
	bool reset = false;
	bool pos = false;
	bool neg = false;
	int state = 0;
    public void Start ()
    {
		lastInterval = Time.realtimeSinceStartup;
		lastTime = Time.realtimeSinceStartup;
        Transform currentParent = transform.parent;
        GameObject camParent = new GameObject ("GyroCamParent");
        camParent.transform.position = transform.position;
        transform.parent = camParent.transform;
        GameObject camGrandparent = new GameObject ("GyroCamGrandParent");
        camGrandparent.transform.position = transform.position;
        camParent.transform.parent = camGrandparent.transform;
        camGrandparent.transform.parent = currentParent;

        #if UNITY_3_4
        gyroBool = Input.isGyroAvailable;
        #else
        gyroBool = SystemInfo.supportsGyroscope;
        #endif
        if (gyroBool) 
		{
            gyro = Input.gyro;
            gyro.enabled = true;
            /*if (Screen.orientation == ScreenOrientation.LandscapeLeft) {
                camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
            } else if (Screen.orientation == ScreenOrientation.Portrait) {
                camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
            } else if (Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
                camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
            } else if (Screen.orientation == ScreenOrientation.LandscapeRight) {
                camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
            } else {
                camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
            }
			
            if (Screen.orientation == ScreenOrientation.LandscapeLeft) {
                rotFix = new Quaternion (0, 0, 1, 0);
            } else if (Screen.orientation == ScreenOrientation.Portrait) {
                rotFix = new Quaternion (0, 0, 1, 0);
            } else if (Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
                rotFix = new Quaternion (0, 0, 1, 0);
            } else if (Screen.orientation == ScreenOrientation.LandscapeRight) {
                rotFix = new Quaternion (0, 0, 1, 0);
            } else {
                rotFix = new Quaternion (0, 0, 1, 0);
            }*/
			camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
			rotFix = new Quaternion (0, 0, 1, 0);
        } else {
            #if UNITY_EDITOR
            print("NO GYRO");
            #endif
        }
		prevRot = this.transform.eulerAngles;
    }
	
    public void FixedUpdate ()
    {
		var timeNow = Time.realtimeSinceStartup;
		float dt = timeNow-lastTime;
		lastTime = timeNow;
		if (timeNow > lastInterval + updateInterval) 
		{
            dtOut = dt;
            lastInterval = timeNow;
        }
        if (gyroBool && this.gameObject.camera.enabled) 
		{
            Quaternion quatMap;
            quatMap = gyro.attitude;
            this.transform.localRotation = quatMap * rotFix;
        }
		//accel zone
		int dx=(int)(prevRot.x-this.transform.eulerAngles.x);
		int dy=(int)(prevRot.y-this.transform.eulerAngles.y);
		int dz=(int)(prevRot.z-this.transform.eulerAngles.z);
		rotationModule = (float)Math.Sqrt(dx*dx + dy*dy + dz*dz)*10000;
		accel = (float)(Round(Input.gyro.userAcceleration.z)*100/(rotationModule<1? 1:rotationModule));
		
		if(accel>0) 
		{
			if(pos && neg && state==2) reset=true;
			if(reset) 
			{
				amount=0;
				state=0;
				reset=false;
				pos=false;
				neg=false;
			}
			pos=true;
			state=1;
		}
		else if(accel<0)
		{
			if(pos && neg && state==1) reset=true;
			if(reset) 
			{
				amount=0;
				state=0;
				reset=false;
				pos=false;
				neg=false;
			}
			neg=true;
			state=2;
		}
		if(pos && neg) 
		{
			if(state==2 && amount>0)
			{
				amount-=accel*dt;
			}
			else if(state==1 && amount<0)
			{
				amount+=accel*dt;
			}
			else{accel=0;}
		}
		else if(pos) amount+=accel*dt;
		else if(neg) amount-=accel*dt;
		
		if(accel>maxAccel) maxAccel=accel;
		if(accel<minAccel) minAccel=accel;
		viscosity = accel==0? 0.01f : 0.0f;
		V = (float)accel*dt + V0;
		if(Mathf.Abs(V)<viscosity) V=0;
		else 
		{
			V += ((V>0) ? (-1) : 1)*viscosity;
		}
		S = (float)V*dt + (float)accel*dt*dt/2;
		float angleY = this.transform.eulerAngles.y;
		Vector3 dir = Vector3.zero;
    	dir.z = S*Mathf.Cos(angleY*Mathf.PI/180);
		dir.x = S*Mathf.Sin(angleY*Mathf.PI/180);
    	this.transform.position = new Vector3(transform.position.x+dir.x, transform.position.y, transform.position.z+dir.z);
		V0 = V;
		prevRot = this.transform.eulerAngles;
    }
	
    float scroll = 180f;

    public void OnGUI ()
    {
		float x = this.transform.eulerAngles.x;
		float y = this.transform.eulerAngles.y;
		float z = this.transform.eulerAngles.z;
		if(x>=180)x=-1*(360-x);
		if(y>=180)y=-1*(360-y);
		if(z>=180)z=-1*(360-z);
		GUI.Label(new Rect(10,80,100,30), System.Convert.ToString((int)x));
		GUI.Label(new Rect(10,130,100,30), System.Convert.ToString((int)y));
		GUI.Label(new Rect(10,180,100,30), System.Convert.ToString((int)z));
		
		GUI.Label(new Rect(10,280,100,30), System.Convert.ToString(Input.gyro.userAcceleration.x));
		GUI.Label(new Rect(10,330,100,30), System.Convert.ToString(Input.gyro.userAcceleration.y));
		GUI.Label(new Rect(10,380,100,30), System.Convert.ToString(Input.gyro.userAcceleration.z));
		
		
		GUI.Label(new Rect(10,580,100,30), "AC: "+System.Convert.ToString(accel));
		GUI.Label(new Rect(10,605,100,30), "RM: "+System.Convert.ToString(rotationModule));
		GUI.Label(new Rect(10,630,100,30), "V: "+System.Convert.ToString(V));
		GUI.Label(new Rect(10,650,100,30), "dt: "+System.Convert.ToString(dtOut));
		GUI.Label(new Rect(10,670,100,30), "MAX: "+System.Convert.ToString(maxAccel));
		GUI.Label(new Rect(10,690,100,30), "MIN: "+System.Convert.ToString(minAccel));
		
		
		
        if (gyroBool && this.gameObject.camera.enabled) 
		{
            scroll = GUI.VerticalScrollbar (new Rect (Screen.width - 35, 25, 30, Screen.height / 2), scroll, 1.0f, 0.0f, 360.0f);
        }

        if (GUI.changed)
		{
            transform.parent.eulerAngles = new Vector3 (transform.parent.eulerAngles.x, scroll, transform.parent.eulerAngles.z);
        }
    }
	
	double Round(double dem)
	{
		dem = dem*20;
		dem = (int)dem;
		dem = dem/20;
		return dem;
	}
}