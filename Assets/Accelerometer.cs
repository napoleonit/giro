using UnityEngine;
using System.Collections;

public class Accelerometer : MonoBehaviour 
{
	float S0 = 0.0f;
	float S = 0.0f;
	float V = 0.0f;
	float V0 = 0.0f;
	float accel = 0.0f;
	
	void Start()
	{
		accel = Input.acceleration.z;
	}
	
	void Update () 
	{
		V = (Input.acceleration.z-accel)*Time.deltaTime + V0;
		S = V*Time.deltaTime + (Input.acceleration.z-accel)*Time.deltaTime*Time.deltaTime/2;
		Vector3 dir = Vector3.zero;
    	dir.x = S;
    	transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z+dir.x);
		S0 = S;
		V0 = V;
	}
}
