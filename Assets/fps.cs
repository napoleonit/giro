using UnityEngine;
using System.Collections;

public class fps : MonoBehaviour 
{
    public float updateInterval = 0.5F;
    private double lastInterval;
    private int frames = 0;
    private float fps2;
    void Start() 
	{
        lastInterval = Time.realtimeSinceStartup;
        frames = 0;
    }
    void OnGUI() 
	{
        GUILayout.Label("" + fps2.ToString("f2"));
    }
    void Update() 
	{
        ++frames;
        float timeNow = Time.realtimeSinceStartup;
        if (timeNow > lastInterval + updateInterval) {
            fps2 = (float)(frames / timeNow - lastInterval);
            frames = 0;
            lastInterval = timeNow;
        }
    }
}