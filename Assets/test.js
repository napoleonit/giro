var speed : float = 10;
var moveThreshold : float = .2;
 
private var movex : float; 
private var iPx : float; 
 
function Update()
{
    movex = 0;
    iPx = iPhoneInput.acceleration.x;
 
    if (Mathf.Abs(iPx) > moveThreshold)
    {
        movex = Mathf.Sign(iPx) * speed;
        transform.Translate(movex,0,0);
    }
}